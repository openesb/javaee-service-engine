/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 1997-2010 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.enterprise.jbi.serviceengine.bridge;

import com.sun.enterprise.config.serverbeans.Application;
import com.sun.enterprise.config.serverbeans.Applications;
import com.sun.enterprise.config.serverbeans.Module;
import com.sun.enterprise.deployment.NameValuePairDescriptor;
import com.sun.enterprise.deployment.WebServiceEndpoint;
import com.sun.enterprise.deployment.WebServicesDescriptor;
import com.sun.enterprise.jbi.serviceengine.core.ServiceEngineEndpoint;
import com.sun.enterprise.jbi.serviceengine.core.EndpointRegistry;
import com.sun.enterprise.jbi.serviceengine.core.JavaEEServiceEngineContext;
import com.sun.enterprise.jbi.serviceengine.core.DescriptorEndpointInfo;
import com.sun.enterprise.jbi.serviceengine.util.JBIConstants;
import org.glassfish.webservices.monitoring.WebServiceEngine;
import org.glassfish.webservices.monitoring.Endpoint;
import org.glassfish.webservices.monitoring.EndpointLifecycleListener;
import org.glassfish.webservices.monitoring.WebServiceEngineImpl;
import com.sun.enterprise.jbi.serviceengine.core.ServiceEngineRuntimeHelper;
import com.sun.enterprise.web.WebApplication;
import com.sun.logging.LogDomains;
import java.util.ArrayList;
import java.util.Collection;
import javax.jbi.component.ComponentContext;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.jbi.JBIException;
import javax.xml.namespace.QName;
import org.glassfish.admin.amx.j2ee.EJBModule;
import org.glassfish.admin.amx.j2ee.J2EEApplication;
import org.glassfish.internal.api.Globals;
import org.glassfish.internal.data.ApplicationInfo;
import org.glassfish.internal.data.ApplicationRegistry;
import org.glassfish.internal.data.EngineRef;
import org.glassfish.internal.data.ModuleInfo;

/**
 * A utillity class which keeps track of JBI enabled end points
 * 
 * @author Manisha Umbarje
 */
public class EndpointHelper {

	/** A web service engine */
	private final WebServiceEngine engine;
	private final ComponentContext context;
	private final EndpointRegistry registry;
	private final Map uriToDetailsMap;
	private static final EndpointHelper helper = new EndpointHelper();
	private final EndpointLifecycleListener epLifecycleListener;
	private static final String auto_enabled = System.getProperty(JBIConstants.AUTO_ENDPOINT_ENABLING);
	/**
	 * Internal handle to the logger instance
	 */
	protected static final Logger logger = LogDomains.getLogger(EndpointHelper.class, LogDomains.SERVER_LOGGER);

	/** Creates a new instance of EndpointHelper */
	private EndpointHelper() {
		engine = WebServiceEngineImpl.getInstance();
		epLifecycleListener = new EndpointLifecycleListenerImpl();
		engine.addLifecycleListener(epLifecycleListener);
		context = JavaEEServiceEngineContext.getInstance().getJBIContext();
		registry = EndpointRegistry.getInstance();
		uriToDetailsMap = new HashMap();
	}

	public static EndpointHelper getInstance() {
		return helper;
	}

	public void initialize() {
		Iterator<Endpoint> endpoints = engine.getEndpoints();
		while (endpoints.hasNext()) {
			registerEndpoint(endpoints.next());
		}
	}

	public void enableEndpoint(QName service, String endpointName) {
		if (endpointName != null) {
			ServiceEngineEndpoint endpoint = registry.get(service, endpointName);
			if (endpoint != null && (!endpoint.isEnabled())) {
				try {
					ServiceEndpoint jbiEndpoint = activateEndpoint(endpoint.getServiceName(), endpoint.getEndpointName());
					endpoint.setServiceEndpoint(jbiEndpoint);
					endpoint.setEnabled(true);
					debug(Level.INFO, "serviceengine.enable_endpoint", new Object[] { service.getLocalPart(), endpointName });
				} catch (Exception e) {
					debug(Level.SEVERE, "serviceengine.error_enable", new Object[] { service.getLocalPart(), endpointName });

				}
			}
		}
	}

	/**
	 * Activates the end point in JBI
	 * 
	 * @param endpoint
	 *            endPoint to be activated in JBI
	 */
	public void registerEndpoint(Endpoint endpoint) {
		registerEndpoint(endpoint.getDescriptor());
	}

	public void registerEndpoint(WebServiceEndpoint webServiceDesc) {
		if (webServiceDesc != null) {
			// activate the end point in JBI
			String endpointName = webServiceDesc.hasWsdlPort() ? webServiceDesc.getWsdlPort().getLocalPart() : webServiceDesc.getEndpointName();

			debug(Level.FINE, "serviceengine.start_registration", new Object[] { webServiceDesc.getServiceName(), endpointName });

			try {

				boolean ejbType = webServiceDesc.implementedByEjbComponent();
				String relativeURI = webServiceDesc.getEndpointAddressUri();
				String implClass = (ejbType) ? webServiceDesc.getTieClassName() : webServiceDesc.getServletImplClass();
				String contextRoot = null;
				com.sun.enterprise.web.WebModule webModule = null;
				if (!ejbType) {
					contextRoot = webServiceDesc.getWebComponentImpl().getWebBundleDescriptor().getContextRoot();
					relativeURI = contextRoot + relativeURI;
					EndpointInfoCollector epInfoCollector = ServiceEngineRuntimeHelper.getRuntime().getEndpointInfoCollector();
					webModule = epInfoCollector.getWebModule(webServiceDesc);
				}

				ServiceEngineEndpoint seEndpoint = new ServiceEngineEndpoint(webServiceDesc, webModule, webServiceDesc.getServiceName(), endpointName,
						implClass, contextRoot, false);
				if (isJBIEnabled(webServiceDesc) || registry.hasProviderEP(seEndpoint)) {
					ServiceEndpoint endpoint = activateEndpoint(webServiceDesc.getServiceName(), endpointName);
					seEndpoint.setServiceEndpoint(endpoint);
                                //        seEndpoint.setEnabled(true);
					debug(Level.FINE, "serviceengine.success_registration", new Object[] { webServiceDesc.getServiceName(), endpointName });
				}/* else {
					seEndpoint.setEnabled(false);
				}*/

				// even if jbi-enabled flag is off, internal registries keep track
				// of deployed web services in SJSAS
				registry.put(webServiceDesc.getServiceName(), endpointName, seEndpoint);

				uriToDetailsMap.put(relativeURI, new Object[] { webServiceDesc.getServiceName(), endpointName });
			} catch (Exception e) {
				debug(Level.SEVERE, "serviceengine.error_registration", new Object[] { webServiceDesc.getServiceName(), endpointName });

			}

		}
	}

	/**
	 * Deactivates the end point in JBI
	 */
	public void disableEndpoint(QName service, String endpointName) {
		// deactivates the end point in JBI
		ServiceEngineEndpoint endpoint = registry.get(service, endpointName);

		if (endpoint != null) {
			try {
				ServiceEndpoint endpt = endpoint.getServiceEndpoint();
				// It's assumed that ServiceEndpoint is priorly activated in JBI
				if (endpt != null) {
					context.deactivateEndpoint(endpt);
					endpoint.setEnabled(false);
					debug(Level.INFO, "serviceengine.disable_endpoint", new Object[] { service.getLocalPart(), endpointName });
				}
			} catch (Exception e) {
				debug(Level.SEVERE, "serviceengine.error_disable", new Object[] { service.getLocalPart(), endpointName });

			}
		}
	}

	public void unregisterEndpoint(QName service, String endpointName) {
		if (endpointName != null) {
			ServiceEngineEndpoint endpoint = registry.get(service, endpointName);

			if (endpoint != null) {
				String endpointURI = endpoint.getURI();
				disableEndpoint(service, endpointName);
				registry.delete(service, endpointName);
				uriToDetailsMap.remove(endpointURI);
				debug(Level.INFO, "serviceengine.success_removal", new Object[] { service.getLocalPart(), endpointName });
			}
		}
	}

	public void toggleEndpointStatus(String uri, boolean flag) {
		Object[] endpointInfo = (Object[]) uriToDetailsMap.get(uri);
		if (endpointInfo != null) {
			if (flag)
				enableEndpoint((QName) endpointInfo[0], (String) endpointInfo[1]);
			else
				disableEndpoint((QName) endpointInfo[0], (String) endpointInfo[1]);
		}

	}

	public void destroy() {
		engine.removeLifecycleListener(epLifecycleListener);
	}

	private ServiceEndpoint activateEndpoint(QName serviceName, String endpointName) throws JBIException {
		String key = DescriptorEndpointInfo.getDEIKey(serviceName, endpointName);
		DescriptorEndpointInfo dei = registry.getJBIEndpts().get(key);
		if (dei != null) {
			serviceName = dei.getServiceName();
			endpointName = dei.getEndpointName();
		}
		return context.activateEndpoint(serviceName, endpointName);
	}

	private void debug(Level logLevel, String msgID, Object[] params) {
		logger.log(logLevel, msgID, params);
	}

	private boolean isJBIEnabled(WebServiceEndpoint endpoint) {
		// TODO: FIX ME
		//return true;
		 try {
		 String applicationName =
		 endpoint.getWebService().getBundleDescriptor().getApplication().getRegistrationName();
		 String endpointName = endpoint.getEndpointName();
                 
                 ApplicationRegistry appRegistry = Globals.getDefaultHabitat().getService(ApplicationRegistry.class);
                 ApplicationInfo appInfo = appRegistry.get(applicationName);

                 List<com.sun.enterprise.deployment.WebServiceEndpoint> webServiceEndpoints = 
                         new ArrayList<com.sun.enterprise.deployment.WebServiceEndpoint>();
                 
                if (appInfo != null) {
                    Collection<ModuleInfo> moduleInfos = appInfo.getModuleInfos();
                    Set<EngineRef> engineRefs = null;
                    for (ModuleInfo moduleInfo : moduleInfos) {
                        engineRefs = moduleInfo.getEngineRefs();
                        for (EngineRef engineRef : engineRefs) {
                            if (engineRef.getApplicationContainer() instanceof WebApplication) {
                                WebApplication webApp = (WebApplication) engineRef.getApplicationContainer();
                                WebServicesDescriptor webServices = (webApp.getDescriptor()).getWebServices();
                                if (webServices != null && !webServiceEndpoints.containsAll(webServices.getEndpoints())) {
                                    webServiceEndpoints.addAll(webServices.getEndpoints());
                                }
                            }
                        }
                    }
                }
                
                for(WebServiceEndpoint endpointBean : webServiceEndpoints) {
                    System.out.println("Endpoint name: " + endpointBean.getName());
                    if (endpointBean.getName().equals(endpointName)) {
                        System.out.println("Endpoint jbi enabled: " + endpointBean);
                        Iterator<NameValuePairDescriptor> ite = endpointBean.getProperties();
                        if (ite != null) {
                            while(ite.hasNext()) {
                                NameValuePairDescriptor nvpd = ite.next();
                                System.out.println("Property: " + nvpd);
                            }
                        }
                        
                        return Boolean.parseBoolean("false");
                    }
                }
                
		           
		 ServiceEngineRuntimeHelper runtimeHelper = ServiceEngineRuntimeHelper.getRuntime();
                 Applications apps = runtimeHelper.getApplications();
 
                 List<Application> applications = apps.getApplications();
                 for(Application app : applications) {
                     if (app.getName().equals(applicationName)) {
                         System.out.println("get endpoint for : " + applicationName);
                         
                         List<org.glassfish.webservices.config.WebServiceEndpoint> endpoints = app.getExtensionsByType(org.glassfish.webservices.config.WebServiceEndpoint.class);
                         
                         System.out.println("Endpoints: " + endpoints.size());
                         if (endpoints != null) {
                             for(org.glassfish.webservices.config.WebServiceEndpoint endpointBean : endpoints) {
                                 System.out.println("Endpoint name: " + endpointBean.getName());
                                 if (endpointBean.getName().equals(endpointName)) {
                                     
                                     System.out.println("Endpoint jbi enabled: " + endpointBean.getJbiEnabled());
                                     return Boolean.parseBoolean(endpointBean.getJbiEnabled());
                                 }
                             }
                         }
                         break;
                     }
                 }
		//
		// // Another way for getting the application reference.
		// String serverInstance = serverContext.getInstanceName();
		// // Application app = configBeansUtility.getSystemApplicationReferencedFrom(serverInstance, applicationName);
		//
		// J2EEApplication app = apps.getModule(J2EEApplication.class, applicationName);
		// if (app != null)
		// webServiceEndpoints = app.getWebServiceEndpoint();
		//
		//                   EJBModule ejbApp = apps.getModule(EJBModule.class, applicationName);
		// if (ejbApp != null)
		// webServiceEndpoints = ejbApp.getWebServiceEndpoint();
		//
		// WebModule webApp = apps.getModule(WebModule.class, applicationName);
		// if (webApp != null)
		// webServiceEndpoints = webApp.getWebServiceEndpoint();
		//
		// if (webServiceEndpoints != null) {
		// Iterator<com.sun.enterprise.config.serverbeans.WebServiceEndpoint> endpoints =
		// webServiceEndpoints.iterator();
		// com.sun.enterprise.config.serverbeans.WebServiceEndpoint endpointBean = null;
		// while (endpoints.hasNext()) {
		// endpointBean = endpoints.next();
		// if ((endpointBean.getName()).equals(endpointName)) {
		// return Boolean.parseBoolean(endpointBean.getJbiEnabled());
		// }
		// }
		// }
		 } catch (Throwable ce) {
		 debug(Level.SEVERE, "serviceengine.config_not_found", new Object[] { endpoint.getServiceName(),
		 endpoint.getEndpointName() });
		 }
		// // By default endpoints are disabled
		 return "true".equalsIgnoreCase(auto_enabled);
	}

}
